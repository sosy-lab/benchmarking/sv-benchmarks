// Copyright (C) 2023 Intel Corporation                                          
//                                                                               
// Permission is hereby granted, free of charge, to any person obtaining a copy  
// of this software and associated documentation files (the "Software"),         
// to deal in the Software without restriction, including without limitation     
// the rights to use, copy, modify, merge, publish, distribute, sublicense,      
// and/or sell copies of the Software, and to permit persons to whom             
// the Software is furnished to do so, subject to the following conditions:      
//                                                                               
// The above copyright notice and this permission notice shall be included       
// in all copies or substantial portions of the Software.                        
//                                                                               
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS       
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL      
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES             
// OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,      
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE            
// OR OTHER DEALINGS IN THE SOFTWARE.                                            
//                                                                               
// SPDX-License-Identifier: MIT

/**
 * @file tdh_mr_finalize_harness.c
 * @brief TDHMRFINALIZE API handler FV harness
 */
#include "tdx_vmm_api_handlers.h"
#include "tdx_basic_defs.h"
#include "auto_gen/tdx_error_codes_defs.h"
#include "x86_defs/x86_defs.h"
#include "data_structures/td_control_structures.h"
#include "memory_handlers/keyhole_manager.h"
#include "memory_handlers/pamt_manager.h"
#include "helpers/helpers.h"
#include "accessors/ia32_accessors.h"
#include "crypto/sha384.h"

#include "fv_utils.h"
#include "fv_env.h"

void tdh_mr_finalize__common_precond() {
    tdx_module_local_t* local_data = get_local_data();
    tdx_leaf_and_version_t leaf_opcode;
    leaf_opcode.raw = local_data->vmm_regs.rax;
    TDXFV_ASSUME(leaf_opcode.leaf == TDH_MR_FINALIZE_LEAF);
}

void tdh_mr_finalize__invalid_input_hkid() {
    tdx_module_local_t* local_data = get_local_data();
    tdx_module_global_t* global_data = get_global_data();

    // Task-specific precondition
    TDXFV_ASSUME(((((pa_t) local_data->vmm_regs.rcx).full_pa & global_data->hkid_mask) >> global_data->hkid_start_bit) != 0); // invalid rcx
    TDXFV_ASSUME(tdr_fv.management_fields.fatal == false);
    TDXFV_ASSUME(tdr_fv.management_fields.lifecycle_state == TD_KEYS_CONFIGURED);
    TDXFV_ASSUME((tdr_fv.management_fields.num_tdcx >= MIN_NUM_TDCS_PAGES) &&
                 (tdr_fv.management_fields.num_tdcx <= MAX_NUM_TDCS_PAGES) &&
                 (tdcs_fv.management_fields.op_state == OP_STATE_INITIALIZED));

    pamt_block_t tdr_pamt_block; // dummy
    pamt_entry_t* tdr_pamt_entry_ptr = NULL;
    bool_t tdr_locked_flag = false; // dummy
    tdr_t* tdr_ptr = NULL; // dummy
    api_error_type map_result = check_lock_and_map_explicit_tdr(
        (pa_t) local_data->vmm_regs.rcx, OPERAND_ID_RCX, 1, TDX_LOCK_EXCLUSIVE, PT_TDR, 
        &tdr_pamt_block, &tdr_pamt_entry_ptr, &tdr_locked_flag, &tdr_ptr
    );
    TDXFV_ASSUME(map_result == TDX_SUCCESS); // XXX potential overconstrain
    TDXFV_ASSUME(tdr_pamt_entry_ptr->pt == PT_TDR);

    // Call ABI function
    local_data->vmm_regs.rax = tdh_mr_finalize(local_data->vmm_regs.rcx);

    // Task-specific postcondition
    TDXFV_ASSERT(local_data->vmm_regs.rax == api_error_with_operand_id(TDX_OPERAND_INVALID, OPERAND_ID_RCX));
}

void tdh_mr_finalize__invalid_state_td_state() {
    tdx_module_local_t* local_data = get_local_data();
    tdx_module_global_t* global_data = get_global_data();

    // Task-specific precondition
    TDXFV_ASSUME(((((pa_t) local_data->vmm_regs.rcx).full_pa & global_data->hkid_mask) >> global_data->hkid_start_bit) == 0);
    TDXFV_ASSUME(tdr_fv.management_fields.fatal == true); // invalid td state
    TDXFV_ASSUME(tdr_fv.management_fields.lifecycle_state == TD_KEYS_CONFIGURED);
    TDXFV_ASSUME((tdr_fv.management_fields.num_tdcx >= MIN_NUM_TDCS_PAGES) &&
                 (tdr_fv.management_fields.num_tdcx <= MAX_NUM_TDCS_PAGES) &&
                 (tdcs_fv.management_fields.op_state == OP_STATE_INITIALIZED));

    pamt_block_t tdr_pamt_block; // dummy
    pamt_entry_t* tdr_pamt_entry_ptr = NULL;
    bool_t tdr_locked_flag = false; // dummy
    tdr_t* tdr_ptr = NULL; // dummy
    api_error_type map_result = check_lock_and_map_explicit_tdr(
        (pa_t) local_data->vmm_regs.rcx, OPERAND_ID_RCX, 1, TDX_LOCK_EXCLUSIVE, PT_TDR, 
        &tdr_pamt_block, &tdr_pamt_entry_ptr, &tdr_locked_flag, &tdr_ptr
    );
    TDXFV_ASSUME(map_result == TDX_SUCCESS); // XXX potential overconstrain
    TDXFV_ASSUME(tdr_pamt_entry_ptr->pt == PT_TDR);

    // Call ABI function
    local_data->vmm_regs.rax = tdh_mr_finalize(local_data->vmm_regs.rcx);

    // Task-specific postcondition
    TDXFV_ASSERT(local_data->vmm_regs.rax == TDX_TD_FATAL);
}

void tdh_mr_finalize__invalid_state_lifecycle() {
    tdx_module_local_t* local_data = get_local_data();
    tdx_module_global_t* global_data = get_global_data();

    // Task-specific precondition
    TDXFV_ASSUME(((((pa_t) local_data->vmm_regs.rcx).full_pa & global_data->hkid_mask) >> global_data->hkid_start_bit) == 0);
    TDXFV_ASSUME(tdr_fv.management_fields.fatal == false);
    TDXFV_ASSUME(tdr_fv.management_fields.lifecycle_state != TD_KEYS_CONFIGURED); // invalid lifecycle
    TDXFV_ASSUME((tdr_fv.management_fields.num_tdcx >= MIN_NUM_TDCS_PAGES) &&
                 (tdr_fv.management_fields.num_tdcx <= MAX_NUM_TDCS_PAGES) &&
                 (tdcs_fv.management_fields.op_state == OP_STATE_INITIALIZED));

    pamt_block_t tdr_pamt_block; // dummy
    pamt_entry_t* tdr_pamt_entry_ptr = NULL;
    bool_t tdr_locked_flag = false; // dummy
    tdr_t* tdr_ptr = NULL; // dummy
    api_error_type map_result = check_lock_and_map_explicit_tdr(
        (pa_t) local_data->vmm_regs.rcx, OPERAND_ID_RCX, 1, TDX_LOCK_EXCLUSIVE, PT_TDR, 
        &tdr_pamt_block, &tdr_pamt_entry_ptr, &tdr_locked_flag, &tdr_ptr
    );
    TDXFV_ASSUME(map_result == TDX_SUCCESS); // XXX potential overconstrain
    TDXFV_ASSUME(tdr_pamt_entry_ptr->pt == PT_TDR);

    // Call ABI function
    local_data->vmm_regs.rax = tdh_mr_finalize(local_data->vmm_regs.rcx);

    // Task-specific postcondition
    TDXFV_ASSERT(local_data->vmm_regs.rax == TDX_TD_KEYS_NOT_CONFIGURED);
}

void tdh_mr_finalize__invalid_state_op_state() {
    tdx_module_local_t* local_data = get_local_data();
    tdx_module_global_t* global_data = get_global_data();

    // Task-specific precondition
    TDXFV_ASSUME(((((pa_t) local_data->vmm_regs.rcx).full_pa & global_data->hkid_mask) >> global_data->hkid_start_bit) == 0);
    TDXFV_ASSUME(tdr_fv.management_fields.fatal == false);
    TDXFV_ASSUME(tdr_fv.management_fields.lifecycle_state == TD_KEYS_CONFIGURED);
    TDXFV_ASSUME(!(tdr_fv.management_fields.num_tdcx >= MIN_NUM_TDCS_PAGES) ||
                 !(tdr_fv.management_fields.num_tdcx <= MAX_NUM_TDCS_PAGES) ||
                 !(tdcs_fv.management_fields.op_state == OP_STATE_INITIALIZED)); // invalid op state

    pamt_block_t tdr_pamt_block; // dummy
    pamt_entry_t* tdr_pamt_entry_ptr = NULL;
    bool_t tdr_locked_flag = false; // dummy
    tdr_t* tdr_ptr = NULL; // dummy
    api_error_type map_result = check_lock_and_map_explicit_tdr(
        (pa_t) local_data->vmm_regs.rcx, OPERAND_ID_RCX, 1, TDX_LOCK_EXCLUSIVE, PT_TDR, 
        &tdr_pamt_block, &tdr_pamt_entry_ptr, &tdr_locked_flag, &tdr_ptr
    );
    TDXFV_ASSUME(map_result == TDX_SUCCESS); // XXX potential overconstrain
    TDXFV_ASSUME(tdr_pamt_entry_ptr->pt == PT_TDR);

    // Call ABI function
    local_data->vmm_regs.rax = tdh_mr_finalize(local_data->vmm_regs.rcx);

    // Task-specific postcondition
    TDXFV_ASSERT(local_data->vmm_regs.rax == TDX_OP_STATE_INCORRECT);
}

void tdh_mr_finalize__invalid_state_tdr_metadata() {
    tdx_module_local_t* local_data = get_local_data();
    tdx_module_global_t* global_data = get_global_data();

    // Task-specific precondition
    TDXFV_ASSUME(((((pa_t) local_data->vmm_regs.rcx).full_pa & global_data->hkid_mask) >> global_data->hkid_start_bit) == 0);
    TDXFV_ASSUME(tdr_fv.management_fields.fatal == false);
    TDXFV_ASSUME(tdr_fv.management_fields.lifecycle_state == TD_KEYS_CONFIGURED);
    TDXFV_ASSUME((tdr_fv.management_fields.num_tdcx >= MIN_NUM_TDCS_PAGES) &&
                 (tdr_fv.management_fields.num_tdcx <= MAX_NUM_TDCS_PAGES) &&
                 (tdcs_fv.management_fields.op_state == OP_STATE_INITIALIZED));

    pamt_block_t tdr_pamt_block; // dummy
    pamt_entry_t* tdr_pamt_entry_ptr = NULL;
    bool_t tdr_locked_flag = false; // dummy
    tdr_t* tdr_ptr = NULL; // dummy
    api_error_type map_result = check_lock_and_map_explicit_tdr(
        (pa_t) local_data->vmm_regs.rcx, OPERAND_ID_RCX, 1, TDX_LOCK_EXCLUSIVE, PT_TDR, 
        &tdr_pamt_block, &tdr_pamt_entry_ptr, &tdr_locked_flag, &tdr_ptr
    );
    TDXFV_ASSUME(map_result == TDX_SUCCESS); // XXX potential overconstrain
    TDXFV_ASSUME(tdr_pamt_entry_ptr->pt != PT_TDR); // invalid metadata

    // Call ABI function
    local_data->vmm_regs.rax = tdh_mr_finalize(local_data->vmm_regs.rcx);

    // Task-specific postcondition
    TDXFV_ASSERT(local_data->vmm_regs.rax == TDX_PAGE_METADATA_INCORRECT);
}

void tdh_mr_finalize__invalid_entry() {
    tdx_module_local_t* local_data = get_local_data();
    tdx_module_global_t* global_data = get_global_data();

    // Task-specific precondition
    bool_t precond = true;
    precond = precond && (((((pa_t) local_data->vmm_regs.rcx).full_pa & global_data->hkid_mask) >> global_data->hkid_start_bit) == 0);
    precond = precond && (tdr_fv.management_fields.fatal == false);
    precond = precond && (tdr_fv.management_fields.lifecycle_state == TD_KEYS_CONFIGURED);
    precond = precond && ((tdr_fv.management_fields.num_tdcx >= MIN_NUM_TDCS_PAGES) &&
                          (tdr_fv.management_fields.num_tdcx <= MAX_NUM_TDCS_PAGES) &&
                          (tdcs_fv.management_fields.op_state == OP_STATE_INITIALIZED));

    pamt_block_t tdr_pamt_block; // dummy
    pamt_entry_t* tdr_pamt_entry_ptr = NULL;
    bool_t tdr_locked_flag = false; // dummy
    tdr_t* tdr_ptr = NULL; // dummy
    api_error_type map_result = check_lock_and_map_explicit_tdr(
        (pa_t) local_data->vmm_regs.rcx, OPERAND_ID_RCX, 1, TDX_LOCK_EXCLUSIVE, PT_TDR, 
        &tdr_pamt_block, &tdr_pamt_entry_ptr, &tdr_locked_flag, &tdr_ptr
    );
    if (map_result == TDX_SUCCESS) {
        precond = precond && (tdr_pamt_entry_ptr->pt == PT_TDR);
    }
    TDXFV_ASSUME(precond == false);

    // Call ABI function
    local_data->vmm_regs.rax = tdh_mr_finalize(local_data->vmm_regs.rcx);

    // Task-specific postcondition
    TDXFV_ASSERT(local_data->vmm_regs.rax != TDX_SUCCESS);
}

void tdh_mr_finalize__valid_entry() {
    tdx_module_local_t* local_data = get_local_data();
    tdx_module_global_t* global_data = get_global_data();

    // Task-specific precondition
    TDXFV_ASSUME(((((pa_t) local_data->vmm_regs.rcx).full_pa & global_data->hkid_mask) >> global_data->hkid_start_bit) == 0);
    TDXFV_ASSUME(tdr_fv.management_fields.fatal == false);
    TDXFV_ASSUME(tdr_fv.management_fields.lifecycle_state == TD_KEYS_CONFIGURED);
    TDXFV_ASSUME((tdr_fv.management_fields.num_tdcx >= MIN_NUM_TDCS_PAGES) &&
                 (tdr_fv.management_fields.num_tdcx <= MAX_NUM_TDCS_PAGES) &&
                 (tdcs_fv.management_fields.op_state == OP_STATE_INITIALIZED));

    pamt_block_t tdr_pamt_block; // dummy
    pamt_entry_t* tdr_pamt_entry_ptr = NULL;
    bool_t tdr_locked_flag = false; // dummy
    tdr_t* tdr_ptr = NULL; // dummy
    api_error_type map_result = check_lock_and_map_explicit_tdr(
        (pa_t) local_data->vmm_regs.rcx, OPERAND_ID_RCX, 1, TDX_LOCK_EXCLUSIVE, PT_TDR, 
        &tdr_pamt_block, &tdr_pamt_entry_ptr, &tdr_locked_flag, &tdr_ptr
    );
    TDXFV_ASSUME(map_result == TDX_SUCCESS); // XXX potential overconstrain
    TDXFV_ASSUME(tdr_pamt_entry_ptr->pt == PT_TDR);

    // Call ABI function
    local_data->vmm_regs.rax = tdh_mr_finalize(local_data->vmm_regs.rcx);

    // Task-specific postcondition
}

void tdh_mr_finalize__free_entry() {
    tdx_module_local_t* local_data = get_local_data();

    // Task-specific precondition

    // Call ABI function
    local_data->vmm_regs.rax = tdh_mr_finalize(local_data->vmm_regs.rcx);

    // Task-specific postcondition
}

void tdh_mr_finalize__post_cover_success() {
    tdx_module_local_t* local_data = get_local_data();
    TDXFV_ASSUME(local_data->vmm_regs.rax == TDX_SUCCESS);

    TDXFV_ASSERT(false);
}

void tdh_mr_finalize__post_cover_unsuccess() {
    tdx_module_local_t* local_data = get_local_data();
    TDXFV_ASSUME(local_data->vmm_regs.rax != TDX_SUCCESS);

    TDXFV_ASSERT(false);
}

void tdh_mr_finalize__common_postcond() {
    tdx_module_local_t* tdx_local_data_ptr = get_local_data();

    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.rax == shadow_td_regs_precall.rax);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.rbx == shadow_td_regs_precall.rbx);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.rcx == shadow_td_regs_precall.rcx);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.rdx == shadow_td_regs_precall.rdx);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.rsp == shadow_td_regs_precall.rsp);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.rbp == shadow_td_regs_precall.rbp);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.rsi == shadow_td_regs_precall.rsi);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.rdi == shadow_td_regs_precall.rdi);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.r8 == shadow_td_regs_precall.r8);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.r9 == shadow_td_regs_precall.r9);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.r10 == shadow_td_regs_precall.r10);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.r11 == shadow_td_regs_precall.r11);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.r12 == shadow_td_regs_precall.r12);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.r13 == shadow_td_regs_precall.r13);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.r14 == shadow_td_regs_precall.r14);
    TDXFV_ASSERT(tdx_local_data_ptr->td_regs.r15 == shadow_td_regs_precall.r15);

    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.rax == shadow_guest_gpr_state_precall.rax);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.rbx == shadow_guest_gpr_state_precall.rbx);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.rcx == shadow_guest_gpr_state_precall.rcx);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.rdx == shadow_guest_gpr_state_precall.rdx);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.rsp == shadow_guest_gpr_state_precall.rsp);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.rbp == shadow_guest_gpr_state_precall.rbp);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.rsi == shadow_guest_gpr_state_precall.rsi);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.rdi == shadow_guest_gpr_state_precall.rdi);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.r8  == shadow_guest_gpr_state_precall.r8);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.r9  == shadow_guest_gpr_state_precall.r9);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.r10 == shadow_guest_gpr_state_precall.r10);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.r11 == shadow_guest_gpr_state_precall.r11);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.r12 == shadow_guest_gpr_state_precall.r12);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.r13 == shadow_guest_gpr_state_precall.r13);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.r14 == shadow_guest_gpr_state_precall.r14);
    TDXFV_ASSERT(tdx_local_data_ptr->vp_ctx.tdvps->guest_state.gpr_state.r15 == shadow_guest_gpr_state_precall.r15);

    //tdx_local_data_ptr->vmm_regs.rax
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.rbx == shadow_vmm_regs_precall.rbx);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.rcx == shadow_vmm_regs_precall.rcx);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.rdx == shadow_vmm_regs_precall.rdx);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.rsp == shadow_vmm_regs_precall.rsp);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.rbp == shadow_vmm_regs_precall.rbp);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.rsi == shadow_vmm_regs_precall.rsi);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.rdi == shadow_vmm_regs_precall.rdi);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.r8 == shadow_vmm_regs_precall.r8);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.r9 == shadow_vmm_regs_precall.r9);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.r10 == shadow_vmm_regs_precall.r10);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.r11 == shadow_vmm_regs_precall.r11);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.r12 == shadow_vmm_regs_precall.r12);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.r13 == shadow_vmm_regs_precall.r13);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.r14 == shadow_vmm_regs_precall.r14);
    TDXFV_ASSERT(tdx_local_data_ptr->vmm_regs.r15 == shadow_vmm_regs_precall.r15);
}
