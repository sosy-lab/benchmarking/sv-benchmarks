# This file is part of the SV-Benchmarks collection of verification tasks:
# https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks
#
# SPDX-FileCopyrightText: 2024 Paulína Ayaziová
#
# SPDX-License-Identifier: GPL-3.0-or-later

This directory contains 6 simple programs and 100 violation witnesses 
in the witness format 2.0. The set is designed to verify that the validator
inspects the input witnesses and supports various features of the witness
format.

 
