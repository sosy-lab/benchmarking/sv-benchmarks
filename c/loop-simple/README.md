<!--
This file is part of the SV-Benchmarks collection of verification tasks:
https://github.com/sosy-lab/sv-benchmarks

SPDX-FileCopyrightText: 2007-2020 The SV-Benchmarks Community, Dirk Beyer <https://www.sosy-lab.org>, Philipp Berger, RWTH Aachen University

SPDX-License-Identifier: Apache-2.0
-->

Simple loop example programs.
 - deep-nested: Intended to show that execution-based witness validators are not always a good solution. Written by Philipp Berger, RWTH Aachen University.
 - nested: loops with constant bounds. Taken from CPAchecker.
